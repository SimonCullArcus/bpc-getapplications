'use strict';
/*jshint esversion: 6 */

const soap = require('soap');
const crypto = require('crypto');
const base64 = require('base-64');
const md5 = require('md5');
const http = require('https');
const querystring = require('querystring');
const AWS = require('aws-sdk');
const syslog = require("syslog-client");
	//EXAMPLE OF EVENT RECEIVED
	//{
//		CustomerName : 'NameOfCustomer',
//		LPAReference : 'X9999',
//		RunStatus: 'Test'
	//};

exports.handler = (event, context, callback) => {
	var S3 = new AWS.S3({apiVersion: '2006-03-01'});
	var LAMBDA = new AWS.Lambda({ region: 'eu-west-1' });
	var DYNAMODBDOCCLIENT = new AWS.DynamoDB.DocumentClient();
	var DYNAMODBRETURN;
	var URLs;

	var client = syslog.createClient('10.42.100.22', {
		  syslogHostname: "cmaas-lambda",
		  transport: syslog.Transport.Tcp,
		  tcpTimeout: 2000
		});

	client.on('error', function(err) {
		console.error('Error from syslog network: %s', err);
		context.fail('Error from syslog network: %s', err);
	});

	var options = {
		   facility: syslog.Facility.Local0,
		   severity: syslog.Severity.Notice
	};


	function ecmMessage(message, endProcess){
		console.log(message);
		client.log(message, options, function(error) {
	         if (error) {
	             console.error(error);
	          }
	         if(endProcess){
	        	 if(message.indexOf('FAILED')>0){
	        		 return context.fail(message);
	        	 }else{
	        		 return context.succeed(message);
	        	 }
	         }
	        });
	}
	
	function GenerateMD5(input){
		return crypto.createHash('md5').update(input).digest('base64');
	}
	
	function uploadToS3(obj){
		return S3.upload(obj).promise();
	}
	
	function getFromS3(fileName){
		return new Promise((resolve,reject)=>{
			S3.getObject({Bucket : 'planningportal-prod', Key: fileName.replace('+',' ')} ,function(err,data){
				if(err){
					//REPORT ERROR
					reject('getFromS3.S3getObject.err: '+ err);
				}else{
					try{
						let file = data.Body.toString('ascii');
						resolve(file);
					}catch(e)
					{
						reject('getFromS3.parseDataToString.err: '+ err);
					}
				}
			});
		});
	}

	function getRequestId(wsdl){
		return new Promise((resolve,reject)=>{
		wsdl.GetNextRequestId({lpaCode : event.LPAReference},function(err, result){
				if(err){
					reject(err);
				}else{
					resolve(result.GetNextRequestIdResult);
				}
			});
		});
	}

	function getFile(wsdl, file, ref){
		return new Promise((success,fail)=>{
			console.log('Getting: '+file.FileName);
		
			getRequestId(wsdl).then(resolve=>{
				let transactionId = resolve;
				
				let queryString = querystring.stringify({
						attachmentId: file.Identifier,
						lpaCode:event.LPAReference,
						requestId:transactionId,
						proposalRef:file.Reference,
						signature:GenerateMD5(file.Identifier+file.Reference+event.LPAReference+DYNAMODBRETURN.LPABCPassword)
					});
				let url = {
						hostname:URLs.Files.hostname,
						path:URLs.Files.path+'?'+queryString
						};
				
				
					try{
						let req = http.request(url,function(res){
							let resChunks = [];
							let resChunksLength = 0;
							res.on('data', function(chunk) {
								resChunks.push(chunk);
								resChunksLength+=chunk.length;
							});
							res.on('end', function(){
								var data = Buffer.concat(resChunks, resChunksLength);
								success(uploadToS3({Bucket : 'planningportal-prod', Key: event.RunStatus+'/'+event.LPAReference+'/Building Control Applications/'+ref+file.FileName, Body: data}));
							});
						});
						req.on('error', function(err){
							fail('req.on.error'+err);
						});
						req.end();
							
					}catch(e){
						fail('try/catch: '+e);
					}
				
			},reject=>{fail('getFile.getRequestId: '+reject);});
		});
	}

	function getProposalList(wsdl){
		return new Promise((success, fail)=>{
			getRequestId(wsdl).then(resolve=>{
				wsdl.GetProposalList({request:{
									LPACode : event.LPAReference,
									RequestId: resolve,
									Signature :GenerateMD5(event.LPAReference+resolve+DYNAMODBRETURN.LPABCPassword)
							}
				},function(err,res){
						if(err){
							fail(err);
						}else{
							if(res.GetProposalListResult.IsSuccess === 'true' && res.GetProposalListResult.Proposals.hasOwnProperty('ProposalSummary')){
								success(res.GetProposalListResult.Proposals.ProposalSummary[0]);
							}else{
								fail('getProposalList: '+res.GetProposalListResult.ErrorMessage.replace('\r\n',', '));
							}
						}
					
				});
			},reject=>{fail('getProposalList.getRequestId: '+reject);});
		});
	}

	function getProposal(wsdl){
		return new Promise((success,fail)=>{
			getProposalList(wsdl).then(resolve=>{
				let application = resolve;
				getRequestId(wsdl).then(resolve=>{
					wsdl.GetProposal({request:{
						ProposalRef: application.ApplicationRef,
						LPACode : event.LPAReference,
						RequestId: resolve,
						Signature :GenerateMD5(application.ApplicationRef+event.LPAReference+resolve+DYNAMODBRETURN.LPABCPassword)
					}},function(err,res){
							if(err){
								fail(err);
							}else{
								if(!res.GetProposalResult.IsSuccess){
									fail(res.GetProposalResult.ErrorMessage);
								}else{
									success(res.GetProposalResult.Proposal);
								}
							}
						});
				},reject=>{fail('getProposal.getRequestId: '+reject);});
				
			},reject=>{fail('getProposal.getProposalList: '+reject);});
		});
	}

	function getFiles(wsdl, fileArray, ref){
		return new Promise ((res,rej)=>{
			const filesPromise = fileArray.reduce((curPromise, file)=>{
				return curPromise
					.then(curFile =>{
						return getFile(wsdl, file, ref)
							.then(resolve=>[...curFile, resolve]);
					});
			}, Promise.resolve([]));
			
			filesPromise.then(results=>{console.log(results);res(results);},err=>rej(err));
		});
	}

	function setProposalReceived(wsdl){
		return new Promise((success, fail)=>{
			getRequestId(wsdl).then(resolve=>{
				wsdl.SetProposalReceived({request:{
									//TBC
							}
				},function(err,res){
						if(err){
							fail(err);
						}else{
							if(res.SetProposalReceivedResponse.IsSuccess === 'true'){
								success(res.SetProposalReceivedResponse.IsSuccess);
							}else{
								fail('setProposalReceived.SetProposalReceived: '+res.SetProposalReceivedResponse.ErrorMessage.replace('\r\n',', '));
							}
						}
					
				});
			},reject=>{fail('setProposalReceived.getRequestId: '+reject);});
		});
	}

	function parseApplication(ref){
		let payload = {
				LPAReference : event.LPAReference,
				CustomerName: event.CustomerName,
				RunStatus: event.RunStatus,
				ProposalRef : ref,
				BucketName : 'planningportal-prod',
				FilePath:  event.RunStatus+'/'+event.LPAReference+'/Building Control Applications/'+ref,
				FileName: 'application.input',
				SalesforceClientId: DYNAMODBRETURN.ppcSalesforceClientId, 
				SalesforceClientSecret : DYNAMODBRETURN.ppcSalesforceClientSecret, 
				SalesforceOrgId : DYNAMODBRETURN.SalesforceOrgId, 
				SalesforcePassword : DYNAMODBRETURN.SalesforcePassword, 
				SalesforceUsername : DYNAMODBRETURN.SalesforceUsername, 
				SalesforceManaged: DYNAMODBRETURN.Managed
				
			};
		console.log(payload);
		return LAMBDA.invoke({
	        FunctionName: 'ppc-parseapplication',
	        Payload: JSON.stringify(payload),
	        InvocationType : 'Event'
		}).promise();
	}

	console.log("--------------------------------------BPC-GETAPPLICATIONS RUNNING----------------------------------------------");
	console.log("------------------------------------------"+new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '')+"---------------------------------------------------");
	console.log(event);
	ecmMessage('Lambda:CMAAS:Integrations:Client: '+event.CustomerName +' - Action: bpc-getapplications - Status: START <'+event.RunStatus+'>',false);
	
	var DYNAMODBPARAMS =  {
			TableName:'customer',
			ProjectionExpression:'LPAReference, LPABCPassword, ppcSalesforceClientId, ppcSalesforceClientSecret, SalesforceOrgId, SalesforcePassword, SalesforceUsername, Managed',
			FilterExpression:'LPAReference = :LPAREF AND RunStatus = :RunStat',
			ExpressionAttributeValues:{ 
				":LPAREF" : event.LPAReference,
				":RunStat": event.RunStatus
			}
		};
		
	DYNAMODBDOCCLIENT.scan(DYNAMODBPARAMS, function(err, data){
		if(err){
			ecmMessage('Lambda:CMAAS:Integrations:Client: '+event.CustomerName +' Action: bpc-getapplications.DynamoDBClient.Scan - Status: FAILED : '+err+'.<'+event.RunStatus+'>',true);
		}
		if(data.Items.length===1){	
			DYNAMODBRETURN= data.Items[0];
			getFromS3('Utility/WSDL/'+event.RunStatus+'.wsdlURLs').then(resolve=>{
				URLs = JSON.parse(resolve);
				soap.createClient(URLs.WSDL, function(err, client){
					getProposal(client,'M3645')
						.then(resolve=>{
							let application = resolve;
							uploadToS3({Bucket : 'planningportal-prod', Key: event.RunStatus+'/'+event.LPAReference+'/Building Control Applications/'+application.GetProposalResult.Proposal.ApplicationHeader.RefNum+'/application.input', Body: JSON.stringify(resolve)})
								.then(resolve=>{
									let filePromises = getFiles(client, application.FileAttachment.FileAttachments, application.GetProposalResult.Proposal.ApplicationHeader.RefNum);
									Promise.all(filePromises)
										.then(values=>{
											setProposalReceived(client)
												.then(resolve=>{
													parseApplication(application.GetProposalResult.Proposal.ApplicationHeader.RefNum)
														.then(resolve=>{
															ecmMessage('Lambda:CMAAS:Integrations:Client: '+event.CustomerName +' - AppRef: '+application.GetProposalResult.Proposal.ApplicationHeader.RefNum+' Action: ppc-getapplicationdetail.confirmProposal - Status: SUCCESS.<'+event.RunStatus+'>',true);
														},reject=>ecmMessage('Lambda:CMAAS:Integrations:Client: '+event.CustomerName +' Action: bpc-getapplications.parseApplication - Status: FAILED : '+reject+'.<'+event.RunStatus+'>',true));									
												},reject=>ecmMessage('Lambda:CMAAS:Integrations:Client: '+event.CustomerName +' Action: bpc-getapplications.setProposalReceived - Status: FAILED : '+reject+'.<'+event.RunStatus+'>',true));
										}, reason=>ecmMessage('Lambda:CMAAS:Integrations:Client: '+event.CustomerName +' Action: bpc-getapplications.FilePromises - Status: FAILED : '+reason+'.<'+event.RunStatus+'>',true));
								},reject=>ecmMessage('Lambda:CMAAS:Integrations:Client: '+event.CustomerName +' Action: bpc-getapplications.uploadToS3 - Status: FAILED : '+reject+'.<'+event.RunStatus+'>',true));
						},reject=>ecmMessage('Lambda:CMAAS:Integrations:Client: '+event.CustomerName +' Action: bpc-getapplications.getProposal - Status: FAILED : '+reject+'.<'+event.RunStatus+'>',true));
				});
				}, reject=>ecmMessage('Lambda:CMAAS:Integrations:Client: '+event.CustomerName +' Action: bpc-getapplications.getFromS3 - Status: FAILED : '+reject+'.<'+event.RunStatus+'>',true));
		}else{
			ecmMessage('Lambda:CMAAS:Integrations:Client: '+event.CustomerName +' Action: bpc-getapplications.DynamoDBClient.Scan - Status: FAILED : Multiple results with reference: '+event.LPAReference+'.<'+event.RunStatus+'>',true);
			}
	});
}
